function [val] = test_clothes(grf, keys)
	val = zeros(size(keys,2));
	for i=1:size(keys,2)
		for j=i+1:size(keys,2)
			key = clo2key(keys(2:3,i), keys(2:3,j));
			if isKey(grf{keys(1,i), keys(1,j)}, key)
				val(i,j) = length(grf{keys(1,i), key(1,j)}(key));
			end
		end
	end
end
