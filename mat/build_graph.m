function [grf] = build_graph(data_dat)
	groups = strsplit(data_dat.items.groups, ',');
	sz_groups = length(groups);
	grf = cell(sz_groups);
	for i=1:sz_groups
		for j=i+1:sz_groups
			grf{i,j} = containers.Map('KeyType', 'double', 'ValueType', 'any');
		end
	end
	for k=1:length(data_dat.clothes)
		if ~data_dat.clothes{k}.crit
			continue;
		end
		for i=1:sz_groups
			for j=i+1:sz_groups
				key = clo2key(data_dat.clothes{k}.data(i,:), data_dat.clothes{k}.data(j,:));
				if key
					if isKey(grf{i,j}, key)
						grf{i,j}(key) = [grf{i,j}(key) data_dat.clothes{k}.file];
					else
						grf{i,j}(key) = {data_dat.clothes{k}.file};
					end
				end
			end
		end
	end
end
