function gf_mathing_gui( )

	gen_fig_name = 'Clothes Matching v.0.1.0';
	data_path = '';
	data_grf = [];
	data_dat = [];
	v1 = [];
	k1 = [];
	f1 = [];
	ind_sel_grp = [];
	
	items = xml_read('Items.xml');

	features = strsplit(items.features, ',');
	groups = strsplit(items.groups, ',');
	groupformat = cellfun(@(x_) ['n/d', strsplit(items.(x_), ',')] ,groups, 'UniformOutput', false);
	featureformat = cellfun(@(x_) ['n/d', strsplit(items.(x_), ',')] ,features, 'UniformOutput', false);

	%% GUI
	mainFigure = figure( ...
		'Name', [gen_fig_name ' ' data_path], ...
		'NumberTitle', 'off', ...
		'MenuBar', 'none', ...
		'Toolbar', 'none', ...
		'HandleVisibility', 'off', ...
		'Units', 'normalized');
	layout0 = uiextras.VBox('Parent', mainFigure, 'Spacing', 3);
	uicontrol('Parent', layout0, 'String', 'Open Data', 'Style', 'pushbutton', 'Callback', @onBtnOpenData);
	layout_grid = uiextras.Grid('Parent', layout0, 'Spacing', 3);
	uicontrol('Parent', layout_grid, 'Style','text', 'String', '');
	for ig=1:length(groups)
		uicontrol('Parent', layout_grid, 'Style','text', 'String', groups{ig});
	end
	uicontrol('Parent', layout_grid, 'Style','text', 'String', 'Design');
	hg = cell(1, length(groups));
	for ig=1:length(groups)
		hg{ig} = uicontrol('Parent', layout_grid, 'Style','popupmenu', 'String', groupformat{ig}, ...
			'Callback', {@onGroups, ig});
	end
	hf = cell(length(features), length(groups));
	for jf=1:length(features)
		uicontrol('Parent', layout_grid, 'Style','text', 'String', features{jf});
		for ig=1:length(groups)
			hf{jf,ig} = uicontrol('Parent', layout_grid, 'Style','popupmenu', 'String', featureformat{jf}, ...
				'Callback', {@onFeatures, jf, ig});
		end
	end
	set( layout_grid , 'ColumnSizes', -ones(1, length(features)+2), 'RowSizes', -ones(1, length(groups)+1) );
	uicontrol('Parent', layout0, 'String', 'Process', 'Style', 'pushbutton', 'Callback', @onBtnProcessData);
	layout01 = uiextras.HBoxFlex('Parent', layout0, 'Spacing', 3);
	table0 = uitable('Parent', layout01);
	layout011 = uiextras.VBox('Parent', layout01, 'Spacing', 3);
	table10 = uitable('Parent', layout011, 'CellEditCallback',{@Ontable10EditCell});
	table11 = uitable('Parent', layout011, 'CellSelectionCallback',{@Ontable11Select});
	set( layout011, 'Sizes', [40 -1] );
%	uicontrol('Parent', layout01, 'String', 'Place for image');
	image = axes('Parent', layout01, 'Visible', 'off');
	set( layout01, 'Sizes', [-1 -1 -1] );
	set( layout0, 'Sizes', [30 (length(groups)+1)*25 30 -1] );

	data_clo = zeros(length(groups), length(features)+1);

	%% events processing
	function onGroups(src, ~, ig)
		data_clo(ig,1) = get(src, 'Value')-1;
	end
	
	function onFeatures(src, ~, jf, ig)
		data_clo(ig,jf+1) = get(src, 'Value')-1;
	end
	
	function onBtnOpenData(~, ~, ~)
		[data_dat, data_path] = read_data_set();
		data_grf = build_graph(data_dat);
		set(mainFigure, 'Name', [gen_fig_name ' ' data_path]);
	end
	
	function onBtnProcessData(~, ~, ~)
		ind = find(data_clo(:,1) ~= 0 & data_clo(:,2)~=0);
		if ~isempty(ind)
			set(table0, 'ColumnName', groups(ind));
			set(table0, 'RowName', groups(ind));
			set(table10, 'ColumnName', groups(ind));
			set(table10, 'ColumnFormat', {groups});
			set(table10, 'ColumnEditable', true);
			set(table10, 'Data', groups(1));
			keys = zeros(3, length(ind));
			for i=1:size(ind)
				keys(1, i) = ind(i);
				keys(2, i) = data_clo(ind(i), 1);
				keys(3, i) = data_clo(ind(i), 2);
			end
 			v0 = test_clothes(data_grf, keys);
 			set(table0, 'Data', v0);
 			[v1, k1, f1] = find_clothes(data_grf, keys);
		end
	end
	
	function Ontable10EditCell(~, eventdata)
		ind_sel_grp = find(strcmp(groups, eventdata.NewData));
		str = arrayfun(@(x_, y_) [num2str(x_) ' : ' num2str(y_)], k1{1,ind_sel_grp}, v1{1,ind_sel_grp}, 'UniformOutput', false);
		set(table11, 'ColumnName', groups(ind_sel_grp));
		set(table11, 'Data', str');
	end
	
	function Ontable11Select(~, eventdata)
		str = eventdata.Source.Data{eventdata.Indices(1)};
		key = str2double(strtok(str));
		[data_i, data_j] = key2clo(key);
 		imshow([data_path filesep f1{ind_sel_grp}{eventdata.Indices(1)}{1}], 'Parent', image);
	end

	
end

