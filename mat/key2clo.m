function [data_i, data_j] = key2clo(key)
	data_j = [fix(key/10^2)-fix(key/10^4)*10^2 key-fix(key/10^2)*10^2];
	data_i = [fix(key/10^6)-fix(key/10^8)*10^2 fix(key/10^4)-fix(key/10^6)*10^2];
end
