function [data_dat, data_path] = read_data_set()
	data_path = uigetdir();
	dd_ = xml_read([data_path filesep 'dataset.xml']);
	data_dat = struct('clothes', {cell(1,length(dd_.clothes))}, 'items', dd_.items);
	for i=1:length(dd_.clothes)
		data_dat.clothes{i} = struct(...
			'file', dd_.clothes(i).file, ...
			'data', dd_.clothes(i).data, ...
			'crit', str2num(dd_.clothes(i).crit)); %#ok<ST2NM>
	end
end
