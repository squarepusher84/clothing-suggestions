function [keys] = keys_clothes(items, clo_set)
 	features = strsplit(items.features, ',');
 	groups = strsplit(items.groups, ',');
 	groupformat = cellfun(@(x_) ['n/d', strsplit(items.(x_), ',')] ,groups, 'UniformOutput', false);
 	featureformat = cellfun(@(x_) ['n/d', strsplit(items.(x_), ',')] ,features, 'UniformOutput', false);
	keys = zeros(3, length(clo_set));
	for i=1:length(clo_set)
		keys(1, i) = find(strcmp(clo_set{i}{1}, groups));
		keys(2, i) = find(strcmp(clo_set{i}{2}, groupformat{keys(1, i)}));
		keys(3, i) = find(strcmp(clo_set{i}{3}, featureformat{1}));
	end
end
