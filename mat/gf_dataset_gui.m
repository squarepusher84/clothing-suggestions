function gf_dataset_gui( )

	gen_fig_name = 'Clothes Set Dataset v.0.2.1';
	data_path = '';
	curr_ind = 0;
	
	items = xml_read('Items.xml');

	features = strsplit(items.features, ',');
	groups = strsplit(items.groups, ',');
	groupformat = cellfun(@(x_) ['n/d', strsplit(items.(x_), ',')] ,groups, 'UniformOutput', false);
	featureformat = cellfun(@(x_) ['n/d', strsplit(items.(x_), ',')] ,features, 'UniformOutput', false);

	%% GUI
	mainFigure = figure( ...
		'Name', [gen_fig_name ' ' data_path], ...
		'NumberTitle', 'off', ...
		'MenuBar', 'none', ...
		'Toolbar', 'none', ...
		'HandleVisibility', 'off', ...
		'Units', 'normalized');
	layout0 = uiextras.VBox('Parent', mainFigure, 'Spacing', 3);
	layout01 = uiextras.HBox('Parent', layout0, 'Spacing', 3);
	uicontrol('Parent', layout01, 'String', 'Open Data', 'Style', 'pushbutton', 'Callback', @onBtnOpenData);
	uicontrol('Parent', layout01, 'String', 'Save Data', 'Style', 'pushbutton', 'Callback', @onBtnSaveData);
	layout02 = uiextras.HBoxFlex('Parent', layout0, 'Spacing', 3);
	table_fc = uitable('Parent', layout02, 'ColumnName', {'file' 'criteria'}, ...
		'ColumnFormat', {'char' 'logical'}, 'ColumnEditable', [false true], ...
		'CellSelectionCallback',{@OnTable02Select});
	layout021 = uiextras.VBoxFlex('Parent', layout02, 'Spacing', 3);
	layout0211 = uiextras.Grid('Parent', layout021, 'Spacing', 3);
	uicontrol('Parent', layout0211, 'Style','text', 'String', '');
	for ig=1:length(groups)
		uicontrol('Parent', layout0211, 'Style','text', 'String', groups{ig});
	end
	uicontrol('Parent', layout0211, 'Style','text', 'String', 'Design');
	hg = cell(1, length(groups));
	for ig=1:length(groups)
		hg{ig} = uicontrol('Parent', layout0211, 'Style','popupmenu', 'String', groupformat{ig}, ...
			'Callback', {@onGroups, ig});
	end
	hf = cell(length(features), length(groups));
	for jf=1:length(features)
		uicontrol('Parent', layout0211, 'Style','text', 'String', features{jf});
		for ig=1:length(groups)
			hf{jf,ig} = uicontrol('Parent', layout0211, 'Style','popupmenu', 'String', featureformat{jf}, ...
				'Callback', {@onFeatures, jf, ig});
		end
	end
	set( layout0211 , 'ColumnSizes', -ones(1, length(features)+2), 'RowSizes', -ones(1, length(groups)+1) );
	image_cl = axes('Parent', layout021, 'Visible', 'off');
	set( layout021, 'Sizes', [(length(groups)+1)*25, -1] );
	set( layout02, 'Sizes', [-1, -4] );
	set( layout0, 'Sizes', [30, -2] );

	data_dat = struct('clothes', {{}}, 'items', items);
	data_clo = zeros(length(groups), length(features)+1);

	%% events processing
	function onGroups(src, ~, ig)
		data_clo(ig,1) = get(src, 'Value')-1;
	end
	
	function onFeatures(src, ~, jf, ig)
		data_clo(ig,jf+1) = get(src, 'Value')-1;
	end
	
	function onBtnOpenData(~, ~, ~)
		data_path = uigetdir('c:\');
		t = dir(data_path);
		data_dat = struct('clothes', {{}}, 'items', items);
		for i=1:length(t)
			if ~t(i).isdir
				try
					imfinfo([data_path filesep t(i).name]);
					data_dat.clothes{end+1} = struct('file', t(i).name, ...
						'data', zeros(length(groups), length(features)+1), ...
						'crit', false);
				catch
				end
			end
		end
		if exist([data_path filesep 'dataset.xml'], 'file')
			data_dat = merge(xml_read([data_path filesep 'dataset.xml']), data_dat);
		end
		set(table_fc, 'Data', [cellfun(@(x_)x_.file, data_dat.clothes, 'UniformOutput', false)' ...
			cellfun(@(x_)x_.crit, data_dat.clothes, 'UniformOutput', false)']);
		set(mainFigure, 'Name', [gen_fig_name ' ' data_path]);
	end
	
	function onBtnSaveData(~, ~, ~)
		tfc = get(table_fc, 'Data');
		if curr_ind ~= 0
			data_dat.clothes{curr_ind}.data = data_clo;
			data_dat.clothes{curr_ind}.crit = tfc{curr_ind,2};
		end
		xml_write([data_path filesep 'dataset.xml'], data_dat);
	end
	
	function OnTable02Select(~, eventdata)
		tfc = get(table_fc, 'Data');
		if curr_ind ~= 0
			data_dat.clothes{curr_ind}.data = data_clo;
			data_dat.clothes{curr_ind}.crit = tfc{curr_ind,2};
		end
		curr_ind = eventdata.Indices(1);
		data_clo = data_dat.clothes{curr_ind}.data;
		for i=1:length(groups)
			set(hg{i}, 'Value', data_clo(i,1)+1);
		end
		for j=1:length(features)
			for i=1:length(groups)
				set(hf{j,i}, 'Value', data_clo(i,j+1)+1);
			end
		end
		imshow([data_path filesep tfc{curr_ind,1}], 'Parent', image_cl);
	end

end

function [dd] = merge(dd_, dd)
	t = isequal(dd_.items, dd.items);
	fs = {dd_.clothes.file};
	for i=1:length(dd.clothes)
		j = find(strcmp(fs, dd.clothes{i}.file));
		if ~isempty(j)
			if t
				dd.clothes{i}.data = dd_.clothes(j).data;
				dd.clothes{i}.crit = str2num(dd_.clothes(j).crit); %#ok<ST2NM>
			else
				error('gf_gui:merge', 'Not Implemented yet');
			end
		end
	end
end

