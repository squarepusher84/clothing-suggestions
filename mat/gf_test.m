function gf_test( )

	%parse input file
	data_dat = read_data_set();

	% build graph
	grf = build_graph(data_dat);
	
	%test graph
	clo_set = {{'OUTWEAR', 'poncho', 'white'}, {'TOPS', 'blouse', 'black'}};
	keys = keys_clothes(data_dat.items, clo_set);
	v0 = test_clothes(grf, keys);
	[v1, k1, f1] = find_clothes(grf, keys);

end
