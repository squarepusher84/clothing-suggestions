function [val, key, fil] = find_clothes(grf, keys)
	val = cell(size(keys,2), size(grf,1));
	key = cell(size(keys,2), size(grf,1));
	fil = cell(size(keys,2), size(grf,1));
	for i=1:size(keys,2)
		for j=1:size(grf,1)
			I = min(keys(1,i), j);
			J = max(keys(1,i), j);
			if (~isempty(grf{I, J}))
				val{i,j} = cellfun(@length,grf{I, J}.values);
				key{i,j} = cell2mat(grf{I, J}.keys);
				fil{i,j} = grf{I, J}.values;
				[val{i,j}, ind] = sort(val{i,j}, 'descend');
				key{i,j} = key{i,j}(ind);
				fil{i,j} = fil{i,j}(ind);
			end
		end
	end
end
