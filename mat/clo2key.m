function [key] = clo2key(data_i, data_j)
	key = 0;
	if data_i(1) && data_i(2) && data_j(1) && data_j(2)
		key = (data_i(1)*100+data_i(2))*10000+(data_j(1)*100+data_j(2));
	end
end
